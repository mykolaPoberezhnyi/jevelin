import Vue from 'vue';
import VueResource from 'vue-resource';
import VueRouter from 'vue-router';
import App from './App.vue';
// components
import Header from './components/Header.vue';
import Products from './components/Products.vue';
import Filters from './components/Filters.vue';
import Template from './components/Template.vue';

Vue.use(VueResource);
Vue.use(VueRouter);

Vue.component('vue-header', Header);
Vue.component('vue-filters', Filters);
Vue.component('vue-products', Products);

var router = new VueRouter({
    routes: [
        { path: '/products', component: Template }
    ]
});

new Vue({
    el: '#app',
    router: router,
    render: h => h(App)
})